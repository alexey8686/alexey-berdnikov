package com.artemis.musala.facade.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.artemis.musala.dto.MedicationDto;
import com.artemis.musala.service.MedicationInteractionService;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MedicationFacadeImplTest {

  @Mock
  private MedicationInteractionService medicationInteractionService;

  @InjectMocks
  private MedicationFacadeImpl medicationFacade;

  @Test
  void getAll() {
    when(medicationInteractionService.getAll()).thenReturn(Collections.emptyList());
    final List<MedicationDto> actual = medicationFacade.getAll();
    assertNotNull(actual);
    verify(medicationInteractionService).getAll();
  }
}