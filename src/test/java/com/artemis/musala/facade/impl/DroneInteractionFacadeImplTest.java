package com.artemis.musala.facade.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.artemis.musala.domain.State;
import com.artemis.musala.dto.DroneDto;
import com.artemis.musala.dto.DroneLoadDto;
import com.artemis.musala.dto.MedicationDto;
import com.artemis.musala.exception.DroneValidationException;
import com.artemis.musala.exception.LowBatteryLevelException;
import com.artemis.musala.service.DroneInteractionService;
import com.artemis.musala.validation.DroneValidationService;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@TestMethodOrder(OrderAnnotation.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
//@Sql(scripts = {"/tes_schema.sql", "/test_data.sql"})
class DroneInteractionFacadeImplTest {

  @Autowired
  private DroneInteractionService droneInteractionService;

  @Autowired
  private DroneValidationService validationService;

  private DroneDto droneDto;


  @BeforeEach
  public void setUp() {
    droneDto = new DroneDto();
    droneDto.setModel("Heavyweight");
    droneDto.setSerialNumber("12");
    droneDto.setWeightLimit(500);
    droneDto.setBatteryCapacity(100);
  }

  @Order(1)
  @Test
  void addNewDroneSuccess() {
    validationService.validate(droneDto);
    final Long actual = droneInteractionService.addNewDrone(droneDto);
    assertNotNull(actual);
    assertTrue(actual != 0L);
  }

  @Order(2)
  @Test
  void addNewDroneValidationError() {
    droneDto.setModel("Heavyweight2");
    assertThrows(DroneValidationException.class,
        () -> validationService.validate(droneDto));
  }


  @Order(3)
  @Test
  void initializeLoadingSuccess() {
    Assertions.assertNull(droneDto.getState());
    final DroneDto actual = droneInteractionService.initializeLoading(droneDto.getSerialNumber());
    assertNotNull(actual);
    assertEquals(actual.getState(), State.LOADING.name());
  }

  @Order(4)
  @Test
  void initializeLoadingLowBattery() {
    droneDto.setModel("Heavyweight");
    droneDto.setSerialNumber("14");
    droneDto.setWeightLimit(500);
    droneDto.setBatteryCapacity(20);
    Assertions.assertNull(droneDto.getState());
    final Long id = droneInteractionService.addNewDrone(droneDto);
    assertThrows(LowBatteryLevelException.class,
        () -> droneInteractionService.initializeLoading(droneDto.getSerialNumber()));

  }

  @Order(5)
  @Test
  void getAvailableForLoading() {
    final List<DroneDto> actual = droneInteractionService.findAllAvailableDrones();
    assertEquals(10, actual.size());
  }

  @Order(6)
  @Test
  void getBatteryCapacityLevel() {
    final Integer actual = droneInteractionService.getBatteryCapacityLevel(
        droneDto.getSerialNumber());
    assertEquals(100, actual);
  }

  @Order(7)
  @Test
  void getLoad() {
    droneInteractionService.initializeLoading(droneDto.getSerialNumber());
    final DroneLoadDto droneLoadDto = new DroneLoadDto();
    final MedicationDto medicationDto = new MedicationDto();
    medicationDto.setCode("S_100");
    medicationDto.setWeight(100);
    droneLoadDto.setMedication(medicationDto);
    droneLoadDto.setCount(1);
    droneInteractionService.update(droneDto.getSerialNumber(),
        Collections.singletonList(droneLoadDto));
    final List<DroneLoadDto> actual = droneInteractionService.getLoad(droneDto.getSerialNumber());
    assertEquals(1, actual.size());
    assertEquals(medicationDto.getCode(), actual.get(0).getMedication().getCode());

  }

  @Order(8)
  @Test
  void unLoad() {
    assertFalse(droneInteractionService.getLoad(droneDto.getSerialNumber()).isEmpty());
    droneInteractionService.unLoad(droneDto.getSerialNumber());
    assertTrue(droneInteractionService.getLoad(droneDto.getSerialNumber()).isEmpty());

  }
}