package com.artemis.musala.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.artemis.musala.dto.DroneStatusEventDto;
import com.artemis.musala.record.DroneFilterRecord;
import com.artemis.musala.record.DroneStateRequestRecord;
import com.artemis.musala.record.PaginationRecord;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

@Sql(scripts = "classpath:droneStateEventData.sql", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, properties = {
    "spring.sql.init.mode=never"})
@ExtendWith(MockitoExtension.class)
class DroneStateEventControllerTest {

  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate testRestTemplate;

  @Test
  void getAll() {
    final ResponseEntity<List> response = this.testRestTemplate.postForEntity(
        "http://localhost:" + port + "/event",
        new DroneStateRequestRecord(null, null, null), List.class);
    final List<DroneStatusEventDto> body = (List<DroneStatusEventDto>) response.getBody();
    assertEquals(3, body.size());
    assertTrue(response.getStatusCode().is2xxSuccessful());
  }

  @Test
  void getFilteredBySerialNumber() {
    final ResponseEntity<List> response = this.testRestTemplate.postForEntity(
        "http://localhost:" + port + "/event",
        new DroneStateRequestRecord(new DroneFilterRecord(null, null, "125"), null, null),
        List.class);
    final List<DroneStatusEventDto> body = (List<DroneStatusEventDto>) response.getBody();
    assertEquals(1, body.size());
    assertTrue(response.getStatusCode().is2xxSuccessful());
  }

  @Test
  void getAllPaginated() {
    final ResponseEntity<List> response = this.testRestTemplate.postForEntity(
        "http://localhost:" + port + "/event",
        new DroneStateRequestRecord(null, null, new PaginationRecord(1, 2)),
        List.class);
    final List<DroneStatusEventDto> body = (List<DroneStatusEventDto>) response.getBody();

    assertEquals(2, body.size());
    assertTrue(response.getStatusCode().is2xxSuccessful());
  }
}