package com.artemis.musala.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.artemis.musala.dto.DroneDto;
import com.artemis.musala.dto.DroneLoadDto;
import com.artemis.musala.facade.DroneInteractionFacade;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class DroneControllerTest {

  private static final String SERIAL = "serial";
  @Mock
  private DroneInteractionFacade droneInteractionFacade;

  @InjectMocks
  private DroneController droneController;

  private int CAPACITY;

  @BeforeEach
  void setUp() {
  }

  @Test
  void create() {
    when(droneInteractionFacade.addNew(any())).thenReturn(1L);
    final ResponseEntity<Long> actual = droneController.create(new DroneDto());
    assertEquals(1L, actual.getBody());
    verify(droneInteractionFacade).addNew(any());
  }

  @Test
  void initializeLoading() {
    when(droneInteractionFacade.initializeLoading(any())).thenReturn(new DroneDto());
    final ResponseEntity<DroneDto> actual = droneController.initializeLoading(
        SERIAL);
    assertNotNull(actual.getBody());
    verify(droneInteractionFacade).initializeLoading(any());
  }

  @Test
  void getLoad() {
    when(droneInteractionFacade.getLoad(any())).thenReturn(
        Collections.singletonList(new DroneLoadDto()));
    final ResponseEntity<List<DroneLoadDto>> actual = droneController.getLoad(SERIAL);
    assertNotNull(actual.getBody());
    verify(droneInteractionFacade).getLoad(any());
  }

  @Test
  void unLoad() {
    when(droneInteractionFacade.unLoad(any())).thenReturn(new DroneDto());
    final ResponseEntity<DroneDto> actual = droneController.unLoad(SERIAL);
    assertEquals(HttpStatus.NO_CONTENT, actual.getStatusCode());
    verify(droneInteractionFacade).unLoad(any());
  }

  @Test
  void update() {
    final ResponseEntity actual = droneController.update(SERIAL,
        Collections.singletonList(new DroneLoadDto()));
    assertEquals(HttpStatus.ACCEPTED, actual.getStatusCode());
    verify(droneInteractionFacade).update(any(), any());

  }

  @Test
  void getAvailableForLoading() {
    when(droneInteractionFacade.getAvailableForLoading()).thenReturn(
        Collections.singletonList(new DroneDto()));
    final ResponseEntity<List<DroneDto>> actual = droneController.getAvailableForLoading();
    assertNotNull(actual.getBody());
    verify(droneInteractionFacade).getAvailableForLoading();
  }

  @Test
  void getBatteryLevel() {
    CAPACITY = 100;
    when(droneInteractionFacade.getBatteryCapacityLevel(any())).thenReturn(CAPACITY);
    final ResponseEntity<Integer> actual = droneController.getBatteryLevel(SERIAL);
    assertEquals(CAPACITY, actual.getBody());
    verify(droneInteractionFacade).getBatteryCapacityLevel(any());
  }
}