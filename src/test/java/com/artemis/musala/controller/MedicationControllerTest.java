package com.artemis.musala.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.artemis.musala.dto.MedicationDto;
import com.artemis.musala.facade.MedicationFacade;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class MedicationControllerTest {

  @Mock
  private MedicationFacade medicationFacade;

  @InjectMocks
  private MedicationController medicationController;


  @Test
  void getAll() {
    when(medicationFacade.getAll()).thenReturn(Collections.emptyList());
    final ResponseEntity<List<MedicationDto>> actual = medicationController.getAll();
    assertNotNull(actual.getBody());
    verify(medicationFacade).getAll();
  }
}