package com.artemis.musala.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.artemis.musala.domain.Drone;
import com.artemis.musala.domain.DroneStatusEvent;
import com.artemis.musala.domain.State;
import com.artemis.musala.repository.DroneRepository;
import com.artemis.musala.repository.DroneStatusEventRepository;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ScheduledDroneEventServiceImplTest {

  @Mock
  private DroneRepository droneRepository;

  @Mock
  private DroneStatusEventRepository eventRepository;

  @InjectMocks
  private ScheduledDroneEventServiceImpl scheduledDroneEventService;

  @Captor
  private ArgumentCaptor<List<DroneStatusEvent>> listArgumentCaptor;

  @Captor
  private ArgumentCaptor<LocalDateTime> dateTimeArgumentCaptor;

  @BeforeEach
  void setUp() {
  }

  @Test
  void checkStatusAndSave() {
    final Drone drone1 = new Drone();
    drone1.setState(State.IDLE);
    drone1.setBatteryCapacity(100);
    drone1.setSerialNumber("1");
    final Drone drone2 = new Drone();
    drone2.setState(State.LOADING);
    drone2.setBatteryCapacity(50);
    drone2.setSerialNumber("2");
    when(droneRepository.findAll()).thenReturn(Arrays.asList(drone1, drone2));
    when(eventRepository.saveAll(listArgumentCaptor.capture())).thenReturn(Collections.emptyList());
    scheduledDroneEventService.checkStatusAndSave();
    assertEquals(drone1.getState().name(), listArgumentCaptor.getValue().get(0).getState());
    assertEquals(drone1.getBatteryCapacity(),
        listArgumentCaptor.getValue().get(0).getBatteryCapacity());
    assertEquals(drone1.getSerialNumber(), listArgumentCaptor.getValue().get(0).getSerialNumber());
    assertEquals(drone2.getState().name(), listArgumentCaptor.getValue().get(1).getState());
    assertEquals(drone2.getBatteryCapacity(),
        listArgumentCaptor.getValue().get(1).getBatteryCapacity());
    assertEquals(drone2.getSerialNumber(), listArgumentCaptor.getValue().get(1).getSerialNumber());
    verify(droneRepository).findAll();
    verify(eventRepository).saveAll(any());
  }

  @Test
  void deleteEvents() {
    doNothing().when(eventRepository)
        .deleteDroneStatusEventByEventDateBefore(dateTimeArgumentCaptor.capture());
    scheduledDroneEventService.deleteEvents();
    verify(eventRepository).deleteDroneStatusEventByEventDateBefore(
        dateTimeArgumentCaptor.getValue());
    assertEquals(LocalDateTime.now().minusDays(3).getDayOfWeek(),
        dateTimeArgumentCaptor.getValue().getDayOfWeek());
  }
}