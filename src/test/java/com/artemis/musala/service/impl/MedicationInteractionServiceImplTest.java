package com.artemis.musala.service.impl;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.artemis.musala.dto.MedicationDto;
import com.artemis.musala.mapper.MedicationMapper;
import com.artemis.musala.repository.MedicationRepository;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

@ExtendWith(MockitoExtension.class)
class MedicationInteractionServiceImplTest {

  @Mock
  private MedicationRepository medicationRepository;

  @Mock
  private MedicationMapper medicationMapper;

  @InjectMocks
  private MedicationInteractionServiceImpl medicationInteractionService;

  @Test
  void getAll() {
    when(medicationRepository.findAll((Sort) any())).thenReturn(null);
    when(medicationMapper.fromList(any())).thenReturn(null);
    final List<MedicationDto> actual = medicationInteractionService.getAll();
    assertNull(actual);
    verify(medicationRepository).findAll((Sort) any());
    verify(medicationMapper).fromList(any());
  }
}