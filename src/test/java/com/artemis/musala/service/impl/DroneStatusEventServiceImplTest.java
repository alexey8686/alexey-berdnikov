package com.artemis.musala.service.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.artemis.musala.domain.DroneStatusEvent;
import com.artemis.musala.dto.DroneStatusEventDto;
import com.artemis.musala.mapper.DroneStatusEventMapper;
import com.artemis.musala.record.DroneStateRequestRecord;
import com.artemis.musala.repository.DroneStatusRepository;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DroneStatusEventServiceImplTest {

  @Mock
  private DroneStatusEventMapper eventMapper;

  @Mock
  private DroneStatusRepository droneStatusRepository;

  @InjectMocks
  private DroneStatusEventServiceImpl droneStatusEventService;

  @Test
  void getFilteredSuccess() {
    when(droneStatusRepository.getFiltered(any())).thenReturn(
        Collections.singletonList(new DroneStatusEvent()));
    when(eventMapper.fromList(any())).thenReturn(
        Collections.singletonList(new DroneStatusEventDto()));
    final List<DroneStatusEventDto> actual = droneStatusEventService.getFiltered(
        new DroneStateRequestRecord(null, null, null));
    assertNotNull(actual);
    verify(droneStatusRepository).getFiltered(any());
    verify(eventMapper).fromList(any());
  }
}