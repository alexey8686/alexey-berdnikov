package com.artemis.musala.aspect;

import com.artemis.musala.exception.DroneNotFoundException;
import com.artemis.musala.exception.DroneValidationException;
import com.artemis.musala.exception.DroneWrongStateException;
import com.artemis.musala.exception.LowBatteryLevelException;
import com.artemis.musala.exception.MedicationNotFoundException;
import com.artemis.musala.exception.TooHeavyLoadException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
@Aspect
public class CatchingAspect {

  private static final Logger LOG = LoggerFactory.getLogger(CatchingAspect.class);

  @Around(value = "@annotation(Catching)")
  public Object executing(ProceedingJoinPoint proceedingJoinPoint) {
    final String methodName = proceedingJoinPoint.getSignature().getName();
    LOG.info("{} - method executing", methodName);
    final Object[] args = proceedingJoinPoint.getArgs();
    try {
      return proceedingJoinPoint.proceed(args);
    } catch (DroneValidationException e) {
      LOG.error(e.getMessage(), e);
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
    } catch (DroneNotFoundException | MedicationNotFoundException e) {
      LOG.error(e.getMessage(), e);
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
    } catch (LowBatteryLevelException e) {
      LOG.error(e.getMessage(), e);
      throw new ResponseStatusException(-100, e.getMessage(), e);
    } catch (TooHeavyLoadException e) {
      LOG.error(e.getMessage(), e);
      throw new ResponseStatusException(-101, e.getMessage(), e);
    } catch (DroneWrongStateException e) {
      LOG.error(e.getMessage(), e);
      throw new ResponseStatusException(-102, e.getMessage(), e);
    } catch (Throwable e) {
      LOG.error(e.getMessage(), e);
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
    }
  }

}
