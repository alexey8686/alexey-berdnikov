package com.artemis.musala.mapper;

import com.artemis.musala.domain.DroneStatusEvent;
import com.artemis.musala.dto.DroneStatusEventDto;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DroneStatusEventMapper {

  DroneStatusEvent to(DroneStatusEventDto statusEventDto);

  DroneStatusEventDto from(DroneStatusEvent statusEvent);

  List<DroneStatusEventDto> fromList(List<DroneStatusEvent> statusEvents);
}
