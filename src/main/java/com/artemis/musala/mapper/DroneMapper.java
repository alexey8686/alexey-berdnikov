package com.artemis.musala.mapper;

import com.artemis.musala.domain.Drone;
import com.artemis.musala.dto.DroneDto;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", imports = MedicationMapper.class)
public interface DroneMapper {

  Drone to(DroneDto droneDto);

  DroneDto from(Drone drone);

  List<DroneDto> fromList(List<Drone> drones);

}
