package com.artemis.musala.mapper;

import com.artemis.musala.domain.Medication;
import com.artemis.musala.dto.MedicationDto;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MedicationMapper {

  Medication to(MedicationDto medicationDto);

  MedicationDto from(Medication medication);

  List<MedicationDto> fromList(List<Medication> medications);
}
