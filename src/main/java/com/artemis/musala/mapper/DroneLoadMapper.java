package com.artemis.musala.mapper;

import com.artemis.musala.domain.DroneLoad;
import com.artemis.musala.dto.DroneLoadDto;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DroneLoadMapper {

  DroneLoad to(DroneLoadDto droneLoadDto);

  DroneLoadDto from(DroneLoad droneLoad);

  List<DroneLoadDto> fromList(List<DroneLoad> droneLoads);
}
