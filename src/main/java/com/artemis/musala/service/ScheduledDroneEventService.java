package com.artemis.musala.service;

public interface ScheduledDroneEventService {

  /**
   * Check drone's status every 50 second and write it to db
   */
  void checkStatusAndSave();


  /**
   * Delete all events older than (current day - 3 days)
   */
  void deleteEvents();

}
