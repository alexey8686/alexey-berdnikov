package com.artemis.musala.service.impl;

import com.artemis.musala.domain.Drone;
import com.artemis.musala.domain.DroneStatusEvent;
import com.artemis.musala.repository.DroneRepository;
import com.artemis.musala.repository.DroneStatusEventRepository;
import com.artemis.musala.service.ScheduledDroneEventService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ScheduledDroneEventServiceImpl implements ScheduledDroneEventService {

  private static final Logger LOG = LoggerFactory.getLogger(ScheduledDroneEventServiceImpl.class);

  private final DroneRepository droneRepository;

  private final DroneStatusEventRepository eventRepository;

  @Scheduled(initialDelay = 20, fixedDelay = 50, timeUnit = TimeUnit.SECONDS)
  @Override
  public void checkStatusAndSave() {
    final List<Drone> drones = droneRepository.findAll();
    List<DroneStatusEvent> droneStatusEvents = new ArrayList<>();
    drones.forEach(drone -> {
      final DroneStatusEvent droneStatusEvent = new DroneStatusEvent();
      droneStatusEvent.setSerialNumber(drone.getSerialNumber());
      droneStatusEvent.setState(drone.getState().name());
      droneStatusEvent.setBatteryCapacity(drone.getBatteryCapacity());
      droneStatusEvents.add(droneStatusEvent);
    });
    eventRepository.saveAll(droneStatusEvents);
  }

  @Scheduled(initialDelay = 1, fixedDelay = 10, timeUnit = TimeUnit.DAYS)
  @Override
  public void deleteEvents() {
    LOG.info("deleting of old records...");
    final LocalDateTime localDateTime = LocalDateTime.now().minusDays(3);
    eventRepository.deleteDroneStatusEventByEventDateBefore(localDateTime);
  }
}

