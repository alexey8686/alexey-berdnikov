package com.artemis.musala.service.impl;

import com.artemis.musala.domain.Medication;
import com.artemis.musala.dto.MedicationDto;
import com.artemis.musala.mapper.MedicationMapper;
import com.artemis.musala.repository.MedicationRepository;
import com.artemis.musala.service.MedicationInteractionService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MedicationInteractionServiceImpl implements
    MedicationInteractionService {

  private final MedicationRepository medicationRepository;

  private final MedicationMapper medicationMapper;

  @Override
  public List<MedicationDto> getAll() {
    final List<Medication> medications = medicationRepository.findAll(Sort.by("name").ascending());
    return medicationMapper.fromList(medications);
  }
}
