package com.artemis.musala.service.impl;

import com.artemis.musala.domain.DroneStatusEvent;
import com.artemis.musala.dto.DroneStatusEventDto;
import com.artemis.musala.mapper.DroneStatusEventMapper;
import com.artemis.musala.record.DroneStateRequestRecord;
import com.artemis.musala.repository.DroneStatusRepository;
import com.artemis.musala.service.DroneStatusEventService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class DroneStatusEventServiceImpl implements DroneStatusEventService {

  private final DroneStatusEventMapper eventMapper;

  private final DroneStatusRepository droneStatusRepository;

  @Override
  public List<DroneStatusEventDto> getFiltered(DroneStateRequestRecord requestRecord) {
    final List<DroneStatusEvent> events = droneStatusRepository.getFiltered(requestRecord);
    return eventMapper.fromList(events);
  }
}
