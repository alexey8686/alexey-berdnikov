package com.artemis.musala.service;

import com.artemis.musala.dto.MedicationDto;
import java.util.List;

public interface MedicationInteractionService {

  /**
   * @return {@link List<MedicationDto>}
   */
  List<MedicationDto> getAll();

}
