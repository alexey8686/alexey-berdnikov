package com.artemis.musala.service;

import com.artemis.musala.dto.DroneStatusEventDto;
import com.artemis.musala.record.DroneStateRequestRecord;
import java.util.List;

public interface DroneStatusEventService {

  /**
   * @param requestRecord -- record with filer, sort, pagination data
   * @return {@link List<DroneStatusEventDto>}
   */
  List<DroneStatusEventDto> getFiltered(DroneStateRequestRecord requestRecord);
}
