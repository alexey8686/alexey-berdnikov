package com.artemis.musala.facade;

import com.artemis.musala.dto.MedicationDto;
import java.util.List;

public interface MedicationFacade {

  /**
   * @return {@link List<MedicationDto>}
   */
  List<MedicationDto> getAll();

}
