package com.artemis.musala.facade;

import com.artemis.musala.dto.DroneDto;
import com.artemis.musala.dto.DroneLoadDto;
import java.util.List;
import org.springframework.web.server.ResponseStatusException;

public interface DroneInteractionFacade {

  /**
   * @param droneDto {@link DroneDto}
   * @return {@link Long}
   * @throws ResponseStatusException -- exception with http status
   */
  Long addNew(final DroneDto droneDto) throws ResponseStatusException;

  /**
   * @param serialNumber {@link String}
   * @return {@link DroneDto}
   * @throws ResponseStatusException -- exception with http status
   */
  DroneDto initializeLoading(final String serialNumber) throws ResponseStatusException;

  /**
   * Load drone with medications
   *
   * @param serialNumber {@link String}
   * @param droneLoad    {@link List<DroneLoadDto>}
   * @throws ResponseStatusException -- exception with http status
   */
  void update(final String serialNumber, final List<DroneLoadDto> droneLoad)
      throws ResponseStatusException;

  /**
   * @return {@link List<DroneLoadDto>}
   * @throws ResponseStatusException -- exception with http status
   */
  List<DroneDto> getAvailableForLoading() throws ResponseStatusException;

  /**
   * @param serialNumber {@link String serialNumber}
   * @return {@link Integer}
   * @throws ResponseStatusException -- exception with http status
   */
  Integer getBatteryCapacityLevel(final String serialNumber) throws ResponseStatusException;

  /**
   * @param serialNumber {@link String}
   * @return {@link List<DroneLoadDto>}
   * @throws ResponseStatusException -- exception with http status
   */
  List<DroneLoadDto> getLoad(final String serialNumber) throws ResponseStatusException;

  /**
   * @param serialNumber {@link String}
   * @return {@link DroneDto}
   */
  DroneDto unLoad(final String serialNumber);
}
