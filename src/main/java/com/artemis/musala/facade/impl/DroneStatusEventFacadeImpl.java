package com.artemis.musala.facade.impl;

import com.artemis.musala.aspect.Catching;
import com.artemis.musala.dto.DroneStatusEventDto;
import com.artemis.musala.facade.DroneStatusEventFacade;
import com.artemis.musala.record.DroneStateRequestRecord;
import com.artemis.musala.service.DroneStatusEventService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DroneStatusEventFacadeImpl implements DroneStatusEventFacade {

  private final DroneStatusEventService droneStatusEventService;

  @Catching
  @Override
  public List<DroneStatusEventDto> getFiltered(DroneStateRequestRecord requestRecord) {
    return droneStatusEventService.getFiltered(requestRecord);
  }
}
