package com.artemis.musala.facade.impl;

import com.artemis.musala.aspect.Catching;
import com.artemis.musala.dto.DroneDto;
import com.artemis.musala.dto.DroneLoadDto;
import com.artemis.musala.facade.DroneInteractionFacade;
import com.artemis.musala.service.DroneInteractionService;
import com.artemis.musala.validation.DroneValidationService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class DroneInteractionFacadeImpl implements DroneInteractionFacade {

  private final DroneInteractionService droneInteractionService;

  private final DroneValidationService validationService;

  @Catching
  @Override
  public Long addNew(final DroneDto droneDto) {
    validationService.validate(droneDto);
    return droneInteractionService.addNewDrone(droneDto);

  }

  @Catching
  @Override
  public DroneDto initializeLoading(final String serialNumber) {
    return droneInteractionService.initializeLoading(serialNumber);
  }

  @Catching
  @Override
  public void update(final String serialNumber, final List<DroneLoadDto> droneLoad) {
    if (droneLoad == null || droneLoad.isEmpty()) {
      return;
    }
    validationService.validate(droneLoad);
    droneInteractionService.update(serialNumber, droneLoad);
  }

  @Catching
  @Override
  public List<DroneDto> getAvailableForLoading() {
    return droneInteractionService.findAllAvailableDrones();
  }

  @Catching
  @Override
  public Integer getBatteryCapacityLevel(final String serialNumber) {
    return droneInteractionService.getBatteryCapacityLevel(serialNumber);
  }

  @Catching
  @Override
  public List<DroneLoadDto> getLoad(final String serialNumber) {
    return droneInteractionService.getLoad(serialNumber);
  }

  @Catching
  @Override
  public DroneDto unLoad(final String serialNumber) {
    return droneInteractionService.unLoad(serialNumber);
  }
}
