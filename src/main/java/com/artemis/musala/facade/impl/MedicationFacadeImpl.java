package com.artemis.musala.facade.impl;

import com.artemis.musala.aspect.Catching;
import com.artemis.musala.dto.MedicationDto;
import com.artemis.musala.facade.MedicationFacade;
import com.artemis.musala.service.MedicationInteractionService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MedicationFacadeImpl implements MedicationFacade {

  private final MedicationInteractionService medicationInteractionService;

  @Catching
  @Override
  public List<MedicationDto> getAll() {
    return medicationInteractionService.getAll();
  }
}
