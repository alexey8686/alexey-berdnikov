package com.artemis.musala.facade;

import com.artemis.musala.dto.DroneStatusEventDto;
import com.artemis.musala.record.DroneStateRequestRecord;
import java.util.List;

public interface DroneStatusEventFacade {

  /**
   * Method lets to get drone's status data with sorting, filtering, pagination
   *
   * @return {@link List<DroneStatusEventDto>}
   */
  List<DroneStatusEventDto> getFiltered(DroneStateRequestRecord requestRecord);

}
