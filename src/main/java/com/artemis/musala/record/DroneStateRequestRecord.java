package com.artemis.musala.record;


/**
 * Request record for drone's status events
 */
public record DroneStateRequestRecord(DroneFilterRecord filter, DroneSortRecord sort,
                                      PaginationRecord paginationRecord) {

}
