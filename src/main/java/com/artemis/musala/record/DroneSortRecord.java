package com.artemis.musala.record;

/**
 * Record for drone's status events sorting
 */
public record DroneSortRecord(String eventDate, String serialNumber) {

}
