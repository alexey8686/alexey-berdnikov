package com.artemis.musala.repository;

import com.artemis.musala.domain.Medication;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Long> {

  Optional<Medication> findFirstByCode(final String code);

}
