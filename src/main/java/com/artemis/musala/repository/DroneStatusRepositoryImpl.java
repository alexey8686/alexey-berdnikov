package com.artemis.musala.repository;

import com.artemis.musala.domain.DroneStatusEvent;
import com.artemis.musala.record.DroneFilterRecord;
import com.artemis.musala.record.DroneSortRecord;
import com.artemis.musala.record.DroneStateRequestRecord;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class DroneStatusRepositoryImpl implements DroneStatusRepository {

  private static final String EVENT_DATE = "eventDate";

  private static final String SERIAL_NUMBER = "serialNumber";

  private static final String DESCENDING_ORDER = "DESC";

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  @Transactional
  public List<DroneStatusEvent> getFiltered(final DroneStateRequestRecord droneStateRequestRecord) {
    final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    final CriteriaQuery<DroneStatusEvent> eventCriteriaQuery = criteriaBuilder.createQuery(
        DroneStatusEvent.class);
    final Root<DroneStatusEvent> eventRoot = eventCriteriaQuery.from(DroneStatusEvent.class);
    eventCriteriaQuery.select(eventRoot);

    final List<Predicate> predicates = constructPredicates(droneStateRequestRecord,
        criteriaBuilder, eventRoot);
    if (!predicates.isEmpty()) {
      eventCriteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
    }

    final List<Order> orders = constructOrders(droneStateRequestRecord, criteriaBuilder, eventRoot);
    eventCriteriaQuery.orderBy(orders);

    final TypedQuery<DroneStatusEvent> typedQuery = entityManager.createQuery(eventCriteriaQuery);

    if (isNeed2Paginate(droneStateRequestRecord)) {
      final Integer pageSize = droneStateRequestRecord.paginationRecord().pageSize();
      final Integer pageNum = droneStateRequestRecord.paginationRecord().pageNum();
      final int firstResult = pageSize * pageNum - pageSize;
      typedQuery.setFirstResult(firstResult).setMaxResults(pageSize);
    }

    return typedQuery.getResultList();
  }

  private boolean isNeed2Paginate(DroneStateRequestRecord droneStateRequestRecord) {
    return droneStateRequestRecord.paginationRecord() != null
        && droneStateRequestRecord.paginationRecord().pageNum() != null
        && droneStateRequestRecord.paginationRecord().pageSize() != null;
  }

  private List<Predicate> constructPredicates(DroneStateRequestRecord droneStateRequestRecord,
      CriteriaBuilder criteriaBuilder, Root<DroneStatusEvent> eventRoot) {
    DroneFilterRecord filterRecord;
    List<Predicate> predicateList = new ArrayList<>();
    if ((filterRecord = droneStateRequestRecord.filter()) != null) {
      if (filterRecord.from() != null) {
        predicateList.add(
            criteriaBuilder.greaterThan(eventRoot.get(EVENT_DATE), filterRecord.from()));
      }
      if (filterRecord.to() != null) {
        predicateList.add(criteriaBuilder.lessThan(eventRoot.get(EVENT_DATE), filterRecord.to()));
      }
      if (filterRecord.serialNumber() != null) {
        predicateList.add(
            criteriaBuilder.equal(eventRoot.get(SERIAL_NUMBER), filterRecord.serialNumber()));
      }
    }
    return predicateList;
  }

  private List<Order> constructOrders(DroneStateRequestRecord droneStateRequestRecord,
      CriteriaBuilder criteriaBuilder, Root<DroneStatusEvent> eventRoot) {
    DroneSortRecord sortRecord;
    List<Order> orderList = new ArrayList<>();

    if ((sortRecord = droneStateRequestRecord.sort()) != null) {
      if (sortRecord.eventDate() != null) {
        orderList.add(
            sortRecord.eventDate().equals(DESCENDING_ORDER) ? criteriaBuilder.desc(
                eventRoot.get(EVENT_DATE))
                : criteriaBuilder.asc(eventRoot.get(EVENT_DATE)));
      }
      if (sortRecord.serialNumber() != null) {
        orderList.add(DESCENDING_ORDER.equals(sortRecord.serialNumber()) ? criteriaBuilder.desc(
            eventRoot.get(SERIAL_NUMBER))
            : criteriaBuilder.asc(eventRoot.get(SERIAL_NUMBER)));
      }

    } else {
      orderList.add(criteriaBuilder.desc(eventRoot.get(EVENT_DATE)));
    }
    return orderList;
  }
}
