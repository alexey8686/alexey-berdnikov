package com.artemis.musala.repository;

import com.artemis.musala.domain.DroneStatusEvent;
import java.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface DroneStatusEventRepository extends JpaRepository<DroneStatusEvent, Long> {

  @Transactional
  void deleteDroneStatusEventByEventDateBefore(LocalDateTime localDateTime);
}
