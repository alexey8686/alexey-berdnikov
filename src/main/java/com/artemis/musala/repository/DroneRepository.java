package com.artemis.musala.repository;

import com.artemis.musala.domain.Drone;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DroneRepository extends JpaRepository<Drone, Long> {

  @Query(value = """
      SELECT * FROM DRONE D WHERE D.SERIAL_NUMBER IN 
      (SELECT D.SERIAL_NUMBER  FROM DRONE D LEFT JOIN DRONE_LOAD LT ON D.ID = LT.DRONE_ID
       LEFT JOIN MEDICATION MED ON LT.MEDICATION_ID = MED.ID
       WHERE (D.STATE IN ('LOADING', 'LOADED') AND D.WEIGHT_LIMIT>= MED.WEIGHT * LT.COUNT OR D.STATE = 'IDLE') 
       AND D.BATTERY_CAPACITY > 25); """, nativeQuery = true)
  List<Drone> findAvailableForLoading();

  @Query(value = "select d.batteryCapacity from Drone d where d.serialNumber =?1")
  Integer getBatteryCapacityLevel(String serialNumber);

  Optional<Drone> findFirstBySerialNumber(String serialNumber);

}
