package com.artemis.musala.repository;

import com.artemis.musala.domain.DroneStatusEvent;
import com.artemis.musala.record.DroneStateRequestRecord;
import java.util.List;

public interface DroneStatusRepository {

  List<DroneStatusEvent> getFiltered(DroneStateRequestRecord droneStateRequestRecord);

}
