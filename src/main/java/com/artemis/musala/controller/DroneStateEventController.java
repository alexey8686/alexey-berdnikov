package com.artemis.musala.controller;

import com.artemis.musala.dto.DroneStatusEventDto;
import com.artemis.musala.facade.DroneStatusEventFacade;
import com.artemis.musala.record.DroneStateRequestRecord;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/event")
public class DroneStateEventController {

  private final DroneStatusEventFacade eventFacade;

  @PostMapping
  public ResponseEntity<List<DroneStatusEventDto>> getFiltered(
      @RequestBody DroneStateRequestRecord requestRecord) {
    return ResponseEntity.ok(eventFacade.getFiltered(requestRecord));
  }

}
