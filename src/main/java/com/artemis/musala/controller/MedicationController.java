package com.artemis.musala.controller;

import com.artemis.musala.dto.MedicationDto;
import com.artemis.musala.facade.MedicationFacade;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/medication")
public class MedicationController {

  private final MedicationFacade medicationFacade;

  @GetMapping
  public ResponseEntity<List<MedicationDto>> getAll() {
    return ResponseEntity.ok(medicationFacade.getAll());
  }

}
