package com.artemis.musala.validation;

import com.artemis.musala.dto.DroneDto;
import com.artemis.musala.dto.DroneLoadDto;
import com.artemis.musala.exception.DroneValidationException;
import java.util.List;

public interface DroneValidationService {

  /**
   * @param droneDto {@link DroneDto}
   * @throws DroneValidationException -- not valid values
   */
  void validate(DroneDto droneDto) throws DroneValidationException;

  /**
   * @param droneLoadDtoList {@link List<DroneLoadDto>} * @throws DroneValidationException -- not
   *                         valid values
   */
  void validate(List<DroneLoadDto> droneLoadDtoList) throws DroneValidationException;

}
