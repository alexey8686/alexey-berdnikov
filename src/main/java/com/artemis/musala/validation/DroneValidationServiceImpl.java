package com.artemis.musala.validation;

import com.artemis.musala.dto.DroneDto;
import com.artemis.musala.dto.DroneLoadDto;
import com.artemis.musala.exception.DroneValidationException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class DroneValidationServiceImpl implements
    DroneValidationService {

  private static final Set<String> models = new HashSet<>();

  static {
    models.add("Lightweight");
    models.add("Middleweight");
    models.add("Cruiserweight");
    models.add("Heavyweight");
  }

  private final Validator validator;

  @Override
  public void validate(DroneDto droneDto) {
    commonValidate(droneDto);
    if (!models.contains(droneDto.getModel())) {
      throw new DroneValidationException(
          "%s - unexpected value of drone's model".formatted(droneDto.getModel()));
    }
  }

  @Override
  public void validate(List<DroneLoadDto> droneLoadDtoList) throws DroneValidationException {
    commonValidate(droneLoadDtoList);
  }

  private void commonValidate(Object object) {
    Set<ConstraintViolation<Object>> violations;
    try {
      violations = validator.validate(object);
    } catch (Exception e) {
      throw new DroneValidationException(e.getMessage(), e);
    }
    if (violations != null && !violations.isEmpty()) {
      throw new DroneValidationException(violations.stream()
          .map(error -> String.format("%s %s", error.getPropertyPath().toString(),
              error.getMessage()))
          .collect(Collectors.joining(",")));
    }
  }


}
