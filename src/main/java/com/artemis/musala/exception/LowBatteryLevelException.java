package com.artemis.musala.exception;

public class LowBatteryLevelException extends RuntimeException {

  public LowBatteryLevelException() {
  }

  public LowBatteryLevelException(String message) {
    super(message);
  }

}
