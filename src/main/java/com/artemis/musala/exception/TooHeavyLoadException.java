package com.artemis.musala.exception;

public class TooHeavyLoadException extends RuntimeException {

  public TooHeavyLoadException() {
  }

  public TooHeavyLoadException(String message) {
    super(message);
  }

}
