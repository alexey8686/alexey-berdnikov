package com.artemis.musala.exception;

public class DroneWrongStateException extends RuntimeException {

  public DroneWrongStateException() {
  }

  public DroneWrongStateException(String message) {
    super(message);
  }
}
