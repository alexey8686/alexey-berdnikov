package com.artemis.musala.exception;

public class DroneValidationException extends RuntimeException {

  public DroneValidationException(String message) {
    super(message);
  }

  public DroneValidationException(String message, Throwable cause) {
    super(message, cause);
  }

}
