package com.artemis.musala.domain;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "DRONE_STATUS_EVENT")
@Getter
@Setter
public class DroneStatusEvent extends IdentifiableEntity {

  @Column(name = "event_Date")
  private LocalDateTime eventDate;

  @Column(name = "battery_capacity")
  private Integer batteryCapacity;

  @Column(name = "serial_number")
  private String serialNumber;

  @Column
  private String state;

  @PrePersist
  protected void onCreate() {
    eventDate = LocalDateTime.now();
  }

}
