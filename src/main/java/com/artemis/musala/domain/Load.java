package com.artemis.musala.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public abstract class Load extends IdentifiableEntity {

  @Column(name = "name")
  private String name;

  @Column(name = "code")
  private String code;

  @Column(name = "weight")
  private Integer weight;

  @Column(name = "imageUrl")
  private String imageUrl;
}
