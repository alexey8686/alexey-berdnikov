package com.artemis.musala.domain;

public interface Identifiable {

  long getId();

}
