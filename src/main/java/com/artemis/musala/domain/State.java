package com.artemis.musala.domain;

import lombok.Getter;

@Getter
public enum State {
  IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING,
}
