package com.artemis.musala.domain;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "DRONE")
public class Drone extends IdentifiableEntity {

  @Column(name = "serial_number")
  private String serialNumber;

  @Column(name = "model")
  private String model;

  @Enumerated(value = EnumType.STRING)
  @Column(name = "state")
  private State state;

  @Column(name = "weight_limit")
  private Integer weightLimit;

  @Column(name = "battery_capacity")
  private Integer batteryCapacity;

  @OneToMany(mappedBy = "drone", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<DroneLoad> droneLoads;

}
