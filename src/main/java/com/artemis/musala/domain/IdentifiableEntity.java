package com.artemis.musala.domain;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public abstract class IdentifiableEntity implements Identifiable {

  @Id
  @SequenceGenerator(name = "global_seq", sequenceName = "global_seq", allocationSize = 1, initialValue = 100)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "global_seq")
  private long id = 0L;

  @Column(name = "version")
  @Version
  private Long version = 0L;
}
