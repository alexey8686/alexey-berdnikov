package com.artemis.musala.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class DroneDto {

  @NotNull(message = "serial number have not to be null")
  @Size(max = 100, message = "max length of serial number have not to be greater then 100 characters")
  @Valid
  private String serialNumber;

  @NotNull(message = "model have not to be null")
  @Valid
  private String model;

  @NotNull(message = "weightLimit have not to be null")
  @Max(value = 500, message = "weight have not to be greater then 500")
  @Valid
  private Integer weightLimit;

  @NotNull(message = "batteryCapacity have not to be null")
  @Valid
  @Max(value = 100, message = "battery capacity have not to be greater then 100%")
  private Integer batteryCapacity;

  private List<DroneLoadDto> droneLoads;

  private String state;
}
