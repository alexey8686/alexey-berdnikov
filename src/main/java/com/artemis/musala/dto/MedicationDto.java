package com.artemis.musala.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class MedicationDto {

  @NotNull
  @Pattern(regexp = "^[0-9A-Za-z_-]*$")
  private String name;

  @NotNull
  @Pattern(regexp = "^[0-9A-Z_]*$")
  private String code;

  @NotNull
  private Integer weight;

  private String imageUrl;

}
