## Build project

from root of project with tests: **mvn clean install**<br/>
from root of project without tests: **mvn -DskipTests clean install**

## Execute project

from folder (target) where jar file is located: **java -jar alexey-berdnikov-1.0.0.jar
com.artemis.musala.DroneApp**

Application will return HttpStatus with body. HttpStatus can vary according to request result.
Custom HttpStatus's codes : <br/> -100 --low battery<br/>
-101 -- too heavy load<br/>
-102 -- wrong drone state for loading

## Add new Drone

POST http://localhost:8080/drone <br/>
Content-Type: application/json

{
"serialNumber": "as_123",
"model": "Lightweight",
"weightLimit": 500,
"batteryCapacity": 100 }

serial number has to be uniq for every drone

## Initialize drone

Move drone to status 'LOADING' and return drone's info

GET http://localhost:8080/drone/{serialNumber}

{serialNumber} - pass serial number of drone to load as path variable

## Load drone

Only drones have bean initialized can be loaded

###

GET http://localhost:8080/medication

- to get list off available to load medications

###

PUT http://localhost:8080/drone/{serialNumber} <br/>
Content-Type: application/json

<pre>
[
    {
        "count": 10,
        "medication": {
            "name": "antibiotic",
            "code": "A_10",
            "weight": 10,
            "imageUrl": "https://images.app.goo.gl/2A4Wg18CRW4k6DQt5"
        } 
    }, 
    {
        "count": 2,
        "medication": {
            "name": "syrup cough",
            "code": "S_100",
            "weight": 100,
            "imageUrl": "https://images.app.goo.gl/X7CkVKc2HYJb5Ppn7"
        } 
    }
]
</pre>

{serialNumber} - pass serial number of drone to load as path variable count - amount of current
medication

## Check drone's load

GET http://localhost:8080/drone/load/{serialNumber}

{serialNumber} - pass serial number of drone to load as path variable

## Unload current drone

DELETE http://localhost:8080/drone/load/{serialNumber}

{serialNumber} - pass serial number of drone to load as path variable

unload drone and move it to IDLE state

## Get all available for loading drones

GET http://localhost:8080/drone

return all drones with status IDLE and all drones with statuses LOADING, LOADED where load's weight
less than drone's weight limit

## Get battery capacity

PATCH http://localhost:8080/drone/{serialNumber}

{serialNumber} - pass serial number of drone to load as path variable

return battery capacity

## Get drone's status events

In app there is scheduler requests all drones data and saves to event journal once per 50 sec. There
is another scheduled task deletes all event older than current date - 3 days once per 10 days

You can filter events by date from/to, by serial number, sort by date and serial number. Also, you
can paginate requested events

POST http://localhost:8080/event <br/>
Content-Type: application/json

<pre>
{
    "filter" : {
        "from" : "2022-10-03T22:30:00",
        "to" : "2022-10-03T23:30:00",
        "serialNumber" : "5"
    },
    "sort":{
        "eventDate" : "DESC",
        "serialNumber" : "DESC"
    },
    "pagination" : {
        "pageNum" : 1,
        "pageSize" : 10 
    } 
}
</pre>
all params are optional